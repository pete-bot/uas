# README #

This is a repo for the UAS software team. 

############################################

To get started you will need:

C++ compiler, such as g++ >=4.8, clang++ >= 3.4 etc, 
cmake, >= 2.8
openCV, >= 3.0.1 

and the flycap SDK >=2.8 (if you intend to run the flycap software)

To compile the required software, navigate to the required directory and run the 'bld' script. This will handle the cmake side of things. 

If you decide to modify the files, that is fine, but if you add any extra, you will need to update the relevant CMakeLists.txt in the top level dir of that package (look at the other files as an example of how to do this, or ask me/google etc)





