// peter kydd pkydd@cse.unsw.edu.au
// 07/02/2016
// program to pull video image data from cham3 camera and produce canny edge emagery. 

#include <iostream>
#include <cstdlib>
#include "FlyCapture2.h"

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>


void showCameraInfo(const FlyCapture2::CameraInfo& camInfo);
void printCameraInfo( const FlyCapture2::CameraInfo& pCamInfo );

int main(int argc, char* argv[] )
{
    

    // establish connection to camera
    FlyCapture2::Error error;
    FlyCapture2::Camera camera;
    FlyCapture2::CameraInfo camInfo;

    // Connect the camera
    // param is PGRGuid* pGuid  - the guid of the imaging device. default is NULL or 0
    error = camera.Connect( 0 );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cerr << "ERROR: Failed to connect to camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // Get the camera info and print it out
    error = camera.GetCameraInfo( &camInfo );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cout << "ERROR: Failed to get camera info from camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // display camera data (not really needed)
    printCameraInfo(camInfo);

    // print out cam res
    //std::cout << "Frame size : " << dWidth << " x " << dHeight << std::endl;
    
    // create viewer window
    cv::namedWindow("Frame",CV_WINDOW_AUTOSIZE); 
    cv::namedWindow("Edges",CV_WINDOW_AUTOSIZE); 
    
    

    // start capturing from device          
    error = camera.StartCapture();
    if ( error == FlyCapture2::PGRERROR_ISOCH_BANDWIDTH_EXCEEDED ){
        std::cout << "ERROR: Bandwidth exceeded." << std::endl;     
        return EXIT_FAILURE;
    } else if ( error != FlyCapture2::PGRERROR_OK ){
        std::cout << "ERROR: Failed to start image capture." << std::endl;     
        return EXIT_FAILURE;
    }          

    cv::Mat edges;
    
    // capture loop
    while(true){
        
        // Get the image
        FlyCapture2::Image rawImage;
        FlyCapture2::Error error = camera.RetrieveBuffer( &rawImage );
        if ( error != FlyCapture2::PGRERROR_OK ){
            std::cout << "ERROR: capture error." << std::endl;
            continue;
        }
        
        // convert to rgb
        FlyCapture2::Image rgbImage;
        rawImage.Convert( FlyCapture2::PIXEL_FORMAT_BGR, &rgbImage );
       
        // convert to OpenCV Mat
        unsigned int rowBytes = (double)rgbImage.GetReceivedDataSize()/(double)rgbImage.GetRows();       
        cv::Mat frame = cv::Mat(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC3, rgbImage.GetData(),rowBytes);
        

        // process frame
        cv::cvtColor(frame, edges, CV_BGR2GRAY);
        cv::GaussianBlur(edges, edges, cv::Size(7,7), 1.5, 1.5);
        cv::Canny(edges, edges, 0, 30, 3);


        // show the frame
        imshow("Frame", frame);
        imshow("Edges", edges);
           
        // allow user to exit capture
        if ( cv::waitKey(1) >= 0 ){
            std::cout << "Stopping capture..." << std::endl;
            break;
        }
    }




    // stop capture
    error = camera.StopCapture();
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cerr << "ERROR: Camera did not stop capture cleanly." << std::endl;
        return EXIT_FAILURE;
    }  

    // clean up camera buffers etc. 
    camera.Disconnect();



    /*
    const unsigned short cam = 1;
    cv::VideoCapture cap(1);
    
    if(!cap.isOpened()){
        std::cerr << "Cannot open video on camera: " << cam << std::endl; 
        abort();
    }   
    */
    

    /*

    // start image processing

    // determine res from camera object - this can be set manually
    double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
    double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

    // print out res
    std::cout << "Frame size : " << dWidth << " x " << dHeight << std::endl;
    
    // create viewer window
    cv::namedWindow("Frame",CV_WINDOW_AUTOSIZE); 
    cv::namedWindow("Edges",CV_WINDOW_AUTOSIZE); 
    
    cv::Mat edges;
    
    while (true){
        
        // create matrix object
        cv::Mat frame;

        // read frame from cam and copy constr into frame. return success bool
        bool bSuccess = cap.read(frame); // read a new frame from video
        if (!bSuccess){
            std::cerr << "Cannot read frame from video stream." << std::endl;
            break;
        }

        // process frame
        cv::cvtColor(frame, edges, CV_BGR2GRAY);
        cv::GaussianBlur(edges, edges, cv::Size(7,7), 1.5, 1.5);
        cv::Canny(edges, edges, 0, 30, 3);


        // show the frame
        imshow("Frame", frame);
        imshow("Edges", edges);

        if ( cv::waitKey(30) >= 0 ){
            std::cout << "esc key is pressed by user" << std::endl;
            break;
        }
    }

    // the camera object is destroyed here by the VideoCapture destructor
    // this should clear the hardware buffer on teh camera (need to check on the cham3)
    cv::destroyAllWindows();

    */

    return EXIT_SUCCESS;
}


void showCameraInfo(const FlyCapture2::CameraInfo& camInfo)
{
    std::cout << "Camera Information: " << std::endl;
    
    std::cout << camInfo.vendorName << " "
              << camInfo.modelName << " " 
              << camInfo.serialNumber << std::endl;
} 
 
    
void printCameraInfo( const FlyCapture2::CameraInfo& pCamInfo )
{
    std::cout << std::endl;
    std::cout << "*** CAMERA INFORMATION ***" << std::endl;
    std::cout << "Serial number -" << pCamInfo.serialNumber << std::endl;
    std::cout << "Camera model - " << pCamInfo.modelName << std::endl;
    std::cout << "Camera vendor - " << pCamInfo.vendorName << std::endl;
    std::cout << "Sensor - " << pCamInfo.sensorInfo << std::endl;
    std::cout << "Resolution - " << pCamInfo.sensorResolution << std::endl;
    std::cout << "Firmware version - " << pCamInfo.firmwareVersion << std::endl;
    std::cout << "Firmware build time - " << pCamInfo.firmwareBuildTime << std::endl << std::endl;

    
}