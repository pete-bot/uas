#!/usr/bin/env python

# code to experiment with video - here we will capture, modify and record

# import our image manip-ulation tools
import numpy as np
import cv2
from matplotlib import pyplot as plt

# capture the video stream from camera (index 0 is default camera, 1 is 1st 2 is 2nd, etc)
capStream = cv2.VideoCapture(0)

# check input stream properties
print capStream.get(3),
print 'x',capStream.get(4)

# modify input stream resolution
capStream.set(3,1024)
capStream.set(4,768)

# set some threshold values for our colour/s of interest
# assumeing YELLOW hi-viz jacket
YELLOW_MIN = np.array([20, 120, 50],np.uint8)
YELLOW_MAX = np.array([40, 255, 255],np.uint8)

# create a videowriter object
#fourcc = cv2.VideoWriter_fourcc(*'XVID')
#out = cv2.VideoWriter('output.avi',fourcc,20.0,(640,480))

while(capStream.isOpened()):
    # capture frame by frame
    ret,frame = capStream.read()
    #ret, frame = capStream.read()
    if ret == True:
    
        # operate on image
        # this flips every frame vertically
        frame = cv2.flip(frame,2)

        # convert from RGB to Hue, Saturation, Value. 
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
        # use gaussian blur/median blur to reduce detail - this will remove noise (may need to be tweaked)
        frame_blur = cv2.medianBlur(frame_hsv, 5)
        frame_blur = cv2.GaussianBlur(frame_hsv, (15,15), 15, frame_blur, 15)

        frame_threshed = cv2.inRange(frame_blur, YELLOW_MIN, YELLOW_MAX)
        cv2.imshow('Threshold', frame_threshed)

        # our operations on the frame come here - convert image to grey (does not work anymore?)
        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # edges = cv2.Canny(frame,100,200)
        # display the resulting frames

        
        # THIS CREATES THE IMAGE OUTPUT FILE
        #out.write(frame)

        # cv2.imshow('frame',frame)
        # cv2.imshow('edges',edges)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# release and close connections
capStream.release()
# out.release()
cv2.destroyAllWindows()