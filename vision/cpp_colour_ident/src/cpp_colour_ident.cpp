// peter kydd pkydd@cse.unsw.edu.au
// 07/02/2016
// program to pull video image data from cham3 camera and produce canny edge emagery. 

#include <iostream>
#include <cstdlib>
#include "FlyCapture2.h"

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>


// colour preset in B,G,R 
//const cv::Scalar YELLOW_MIN = cv::Scalar(20, 100, 100);
//const cv::Scalar YELLOW_MAX = cv::Scalar(30, 255, 255);

const cv::Scalar YELLOW_MIN = cv::Scalar(20, 120, 50);
//const cv::Scalar YELLOW_MIN = cv::Scalar(40, 230, 230);
const cv::Scalar YELLOW_MAX = cv::Scalar(40, 255, 255);

cv::Mat drawCrosshairs(cv::Mat input);
void showYellowTestBox(void);
void showCameraInfo(const FlyCapture2::CameraInfo& camInfo);
void printCameraInfo( const FlyCapture2::CameraInfo& pCamInfo );



int main(int argc, char* argv[] )
{

    // establish connection to camera
    FlyCapture2::Error error;
    FlyCapture2::Camera camera;
    FlyCapture2::CameraInfo camInfo;

    // Connect the camera
    // param is PGRGuid* pGuid  - the guid of the imaging device. default is NULL or 0 (for first cam)
    error = camera.Connect( 0 );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cerr << "ERROR: Failed to connect to camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // Get the camera info and print it out
    error = camera.GetCameraInfo( &camInfo );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cout << "ERROR: Failed to get camera info from camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // display camera data (not really needed)
    printCameraInfo(camInfo);

    // start capturing from device          
    error = camera.StartCapture();
    if ( error == FlyCapture2::PGRERROR_ISOCH_BANDWIDTH_EXCEEDED ){
        std::cout << "ERROR: Bandwidth exceeded." << std::endl;     
        return EXIT_FAILURE;
    } else if ( error != FlyCapture2::PGRERROR_OK ){
        std::cout << "ERROR: Failed to start image capture." << std::endl;     
        return EXIT_FAILURE;
    }          


    // create viewer window (resizable)
    cv::namedWindow("input",CV_WINDOW_NORMAL);
    cv::namedWindow("thresh_YELLOW",CV_WINDOW_NORMAL);
    
    //cv::namedWindow("YELLOW_MIN", CV_WINDOW_NORMAL);
    //cv::namedWindow("YELLOW_MAX", CV_WINDOW_NORMAL);

    
    /*

        cv::namedWindow("hue",CV_WINDOW_NORMAL);
        cv::namedWindow("sat",CV_WINDOW_NORMAL);
        cv::namedWindow("val",CV_WINDOW_NORMAL); 
        //cv::namedWindow("blurredHue",CV_WINDOW_NORMAL); 
        //cv::namedWindow("outputH",CV_WINDOW_NORMAL); 
        //cv::namedWindow("cannyH",CV_WINDOW_NORMAL);
    */

    cv::Mat edges;
    



    // capture loop
    while(true){
        
        // get and covert image from flycap chameleon to opencv format
        // Get the image        
        FlyCapture2::Image rawImage;
        FlyCapture2::Error error = camera.RetrieveBuffer( &rawImage );
        if ( error != FlyCapture2::PGRERROR_OK ){
            std::cout << "ERROR: capture error." << std::endl;
            continue;
        }
        
        // convert to rgb
        FlyCapture2::Image rgbImage;
        rawImage.Convert( FlyCapture2::PIXEL_FORMAT_BGR, &rgbImage );
       
        // convert to OpenCV Mat
        unsigned int rowBytes = (double)rgbImage.GetReceivedDataSize()/(double)rgbImage.GetRows();       
        cv::Mat input;
        input = cv::Mat(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC3, rgbImage.GetData(),rowBytes);


 
        // check threshold data
        cv::Mat hsv;
        cv::cvtColor(input,hsv,CV_BGR2HSV);

        cv::Mat threshImage;
        cv::inRange(hsv, YELLOW_MIN, YELLOW_MAX, threshImage);


        // draw crosshair
        input = drawCrosshairs(input);
        imshow("input", input);
        imshow("thresh_YELLOW", threshImage);

        //showYellowTestBox();



        // TODO:
        // get edge from threshold image
        //




        // allow user to exit capture
        short c = cv::waitKey(1); 
        if ( c == 'q' ){
            std::cout << "Stopping capture..." << std::endl;
            break;
        }
    }

    // stop capture
    error = camera.StopCapture();
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cerr << "ERROR: Camera did not stop capture cleanly." << std::endl;
        return EXIT_FAILURE;
    }  

    // clean up camera buffers etc and disconnect camera from camera object. 
    camera.Disconnect();


    return EXIT_SUCCESS;
}




cv::Mat drawCrosshairs(cv::Mat input)
{

    cv::Scalar red(0,0,255);
    //crosshair horizontal
    cv::line(input, cv::Point((input.cols/2)-40, input.rows/2), cv::Point((input.cols/2)+40, input.rows/2), red, 1, cv::LINE_4,0);  

    //crosshair vertical
    cv::line(input, cv::Point(input.cols/2, (input.rows/2)-40), cv::Point(input.cols/2, (input.rows/2)+40), red, 1, cv::LINE_4,0);  

    return input;
}






void showYellowTestBox(void)
{
    cv::Mat yellow_min(50,50,CV_8UC3, YELLOW_MIN);
    cv::Mat yellow_max(50,50,CV_8UC3, YELLOW_MAX);
    imshow("YELLOW_MIN", yellow_min);
    imshow("YELLOW_MAX", yellow_max);
}


void showCameraInfo(const FlyCapture2::CameraInfo& camInfo)
{
    std::cout << "Camera Information: " << std::endl;
    
    std::cout << camInfo.vendorName << " "
              << camInfo.modelName << " " 
              << camInfo.serialNumber << std::endl;
} 
 


    
void printCameraInfo( const FlyCapture2::CameraInfo& pCamInfo )
{
    std::cout << std::endl;
    std::cout << "*** CAMERA INFORMATION ***" << std::endl;
    std::cout << "Serial number - " << pCamInfo.serialNumber << std::endl;
    std::cout << "Camera model - " << pCamInfo.modelName << std::endl;
    std::cout << "Camera vendor - " << pCamInfo.vendorName << std::endl;
    std::cout << "Sensor - " << pCamInfo.sensorInfo << std::endl;
    std::cout << "Resolution - " << pCamInfo.sensorResolution << std::endl;
    std::cout << "Firmware version - " << pCamInfo.firmwareVersion << std::endl;
    std::cout << "Firmware build time - " << pCamInfo.firmwareBuildTime << std::endl << std::endl;

    
}

