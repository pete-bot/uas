// peter kydd pkydd@cse.unsw.edu.au
// 07/02/2016
// program to pull video image data from cham3 camera and produce canny edge emagery. 

#include <iostream>
#include <cstdlib>
#include "FlyCapture2.h"

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>




int main(int argc, char* argv[] )
{

    std::cout << "Entering loop..." << std::endl;
    
    while(true){
           

          //namedWindow("Image anycolor",WINDOW_NORMAL);
        cv::Mat frame;
        frame = cv::imread("test.png",CV_LOAD_IMAGE_UNCHANGED);
        cv::imshow("test Image", frame);
    


        // allow user to exit capture
        if ( cv::waitKey(1) == 'q' ){
            std::cout << "Stopping capture..." << std::endl;
            break;
        }
    }
    std::cout << "Exiting loop." << std::endl;



    return EXIT_SUCCESS;
}
