// Copyright (c) 2008, Sebastian Montabone
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of the owner nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// 
// File:   main.cpp
// Author: Sebastian Montabone <samontab at puc.cl>
//
// Created on September 24, 2008, 2:57 PM
//
// Fine-grained Saliency library (FGS). 
// http://www.samontab.com/web/saliency/
// This library calculates fine grained saliency in real time using integral images.
// It requires OpenCV.
//
#include <stdio.h>
#include "saliency.h"
#include <highgui.h>

bool running = true;
void readInput();

int main(int argc, char** argv)
{
	char filename[200];
	IplImage *srcImg, *dstImg;
	char *win1 = "Source"; 
	char *win2 = "Saliency"; 


	if(argc > 1)
		sprintf(filename, "%s", argv[1]);
	else
		sprintf(filename, "lena.jpg");

	srcImg = cvLoadImage(filename);
	dstImg = cvCreateImage(cvSize(srcImg->width, srcImg->height), 8, 1);



	//Create windows
	cvNamedWindow(win1);
	cvMoveWindow(win1, 0, 100);
	cvNamedWindow(win2);
	cvMoveWindow(win2, srcImg->width + 10, 100);



	Saliency *saliency = new Saliency;
	
	double t = (double)cvGetTickCount();
	//get saliency
	saliency->calcIntensityChannel(srcImg, dstImg);

	cvSaveImage("saliency.jpg", dstImg);
//	IplImage * im2 = cvCreateImage(cvSize(srcImg->width/8, srcImg->height/8), 8, 1);
//	cvResize(dstImg, im2);
//	cvResize(im2, dstImg);
//	cvReleaseImage(&im2);

	t = (double)cvGetTickCount() - t;
	
	printf( "Image processed in %gms\n", t/((double)cvGetTickFrequency()*1000.) );
	printf( "Press ESC to exit.\n");

	cvShowImage(win1, srcImg);
	cvShowImage(win2, dstImg);

	while(running)
	{
		//read user input
		readInput();
	}


	// release resources
	cvReleaseImage(&srcImg);	
	cvReleaseImage(&dstImg);
	cvDestroyAllWindows();

	return 0;
}


void readInput()
{
	char keyPressed = cvWaitKey(10);

	switch (keyPressed)
	{
	case 27: //code for ESC key
		{
			running = false;
			break;
		}
	}
}
