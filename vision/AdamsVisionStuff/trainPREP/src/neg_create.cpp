#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <dirent.h>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

void partition(cv::Mat image, std::string basename);
std::vector<std::string> readDirectory();


int main(int argc, char** argv )
{
	
	std::vector<std::string> file_names;

	file_names = readDirectory();
	
	for(unsigned int i = 0; i < file_names.size(); i++) {
		cv::Mat loadImage;
		loadImage = cv::imread("neg_data/" + file_names[i],CV_LOAD_IMAGE_COLOR);
		if(!loadImage.data) {
			std::cout << "Could not read: " + file_names[i] << std::endl;
			continue;
		}
		
		std::string ext = ".jpg";
		std::string basename = file_names[i];
		std::string::size_type pos = basename.find(ext);
		basename.erase(pos, ext.length());
		
		partition(loadImage,basename);
		
		
	}
	
	
	
	//cv::Mat loadImage;
	//loadImage = cv::imread("neg_data/n1.jpg");
	//partition(loadImage);

}

void partition(cv::Mat image, std::string basename) 
{
	int height = image.rows;
	int width = image.cols;	
	int block_count = 0;
	int jmp = 12;
	
	for (int i = 0; i < height - 128 - jmp; i+=jmp) {
		
		for(int j = 0; j < width - 64 - jmp ; j+=jmp) {
			cv::Mat sample(image, cv::Rect(j,i,64,128));
			cv::imwrite("neg_crop/"+basename+ "_" +std::to_string(i)+"_"+std::to_string(j)+".jpg", sample); 
			block_count++;
		}
		
	}	
	
	std::cout << basename + ": " + std::to_string(block_count) << std::endl;
	
}

std::vector<std::string> readDirectory() {
	
	DIR *dpdf;
	struct dirent *epdf;
	std::vector<std::string> names;
	
	dpdf = opendir("./neg_data/");
	if (dpdf != NULL){
		while ( (epdf = readdir(dpdf)) ){
			std::string file_name = epdf->d_name;
			if(file_name.find( "jpg" ) != std::string::npos) {
				names.push_back(file_name);
			}
	  }
	}
	closedir(dpdf);
	
	return names;
	
}
