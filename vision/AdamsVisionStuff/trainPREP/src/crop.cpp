#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <dirent.h>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

void cropImage(cv::Mat& image, cv::Mat& output);
std::vector<std::string> readDirectory();


int main(int argc, char** argv )
{
	std::vector<std::string> file_names;

	file_names = readDirectory();
	
	for(unsigned int i = 0; i < file_names.size(); i++) {
		cv::Mat loadImage;
		loadImage = cv::imread("data/" + file_names[i],CV_LOAD_IMAGE_COLOR);
		if(!loadImage.data) {
			std::cout << "Could not read: " + file_names[i] << std::endl;
			continue;
		}
		
		cv::Mat cropped;	
		cropImage(loadImage,cropped);
		cv::Size size(48,68);
		cv::resize(cropped, cropped, size);
		
	
		std::string ext = ".png";
		std::string basename = file_names[i];
		std::string::size_type pos = basename.find(ext);
		basename.erase(pos, ext.length());
		
		cv::imwrite("cropped/"+basename+".jpg", cropped);
		
	}
	

}

void cropImage(cv::Mat& image, cv::Mat& output)
{
	
	cv::Mat loadedImage;	
	image.copyTo(loadedImage);
	
	cv::cvtColor(loadedImage,loadedImage,CV_BGR2GRAY);
	cv::threshold(loadedImage,loadedImage,1,255,CV_THRESH_BINARY);
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	findContours(loadedImage,contours,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE);
	std::vector<cv::Point> contours_poly (contours.size());
	cv::Rect boundRect;
	approxPolyDP(cv::Mat(contours[0]), contours_poly, 3, true);
	boundRect = boundingRect(cv::Mat(contours_poly));
	
	boundRect.width -= 6;
	boundRect.height -= 9;
	
	cv::Point top = boundRect.tl();
	top.x += 4;
	top.y += 4;
	boundRect = cv::Rect(top,boundRect.br());
	
	cv::Mat crop;
	image(boundRect).copyTo(crop);	
		
	output = crop;
		
}

std::vector<std::string> readDirectory() {
	
	DIR *dpdf;
	struct dirent *epdf;
	std::vector<std::string> names;
	
	dpdf = opendir("./data/");
	if (dpdf != NULL){
		while ( (epdf = readdir(dpdf)) ){
			std::string file_name = epdf->d_name;
			if(file_name.find( "png" ) != std::string::npos) {
				names.push_back(file_name);
			}
	  }
	}
	closedir(dpdf);
	
	return names;
	
}
