# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/adam/Dropbox/UAS/uas/trainPREP

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/adam/Dropbox/UAS/uas/trainPREP/build

# Include any dependencies generated for this target.
include CMakeFiles/crop.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/crop.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/crop.dir/flags.make

CMakeFiles/crop.dir/src/crop.cpp.o: CMakeFiles/crop.dir/flags.make
CMakeFiles/crop.dir/src/crop.cpp.o: ../src/crop.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/adam/Dropbox/UAS/uas/trainPREP/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/crop.dir/src/crop.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/crop.dir/src/crop.cpp.o -c /home/adam/Dropbox/UAS/uas/trainPREP/src/crop.cpp

CMakeFiles/crop.dir/src/crop.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/crop.dir/src/crop.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/adam/Dropbox/UAS/uas/trainPREP/src/crop.cpp > CMakeFiles/crop.dir/src/crop.cpp.i

CMakeFiles/crop.dir/src/crop.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/crop.dir/src/crop.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/adam/Dropbox/UAS/uas/trainPREP/src/crop.cpp -o CMakeFiles/crop.dir/src/crop.cpp.s

CMakeFiles/crop.dir/src/crop.cpp.o.requires:
.PHONY : CMakeFiles/crop.dir/src/crop.cpp.o.requires

CMakeFiles/crop.dir/src/crop.cpp.o.provides: CMakeFiles/crop.dir/src/crop.cpp.o.requires
	$(MAKE) -f CMakeFiles/crop.dir/build.make CMakeFiles/crop.dir/src/crop.cpp.o.provides.build
.PHONY : CMakeFiles/crop.dir/src/crop.cpp.o.provides

CMakeFiles/crop.dir/src/crop.cpp.o.provides.build: CMakeFiles/crop.dir/src/crop.cpp.o

# Object files for target crop
crop_OBJECTS = \
"CMakeFiles/crop.dir/src/crop.cpp.o"

# External object files for target crop
crop_EXTERNAL_OBJECTS =

../crop: CMakeFiles/crop.dir/src/crop.cpp.o
../crop: CMakeFiles/crop.dir/build.make
../crop: /usr/local/lib/libopencv_xphoto.so.3.1.0
../crop: /usr/local/lib/libopencv_xobjdetect.so.3.1.0
../crop: /usr/local/lib/libopencv_ximgproc.so.3.1.0
../crop: /usr/local/lib/libopencv_xfeatures2d.so.3.1.0
../crop: /usr/local/lib/libopencv_tracking.so.3.1.0
../crop: /usr/local/lib/libopencv_text.so.3.1.0
../crop: /usr/local/lib/libopencv_surface_matching.so.3.1.0
../crop: /usr/local/lib/libopencv_structured_light.so.3.1.0
../crop: /usr/local/lib/libopencv_stereo.so.3.1.0
../crop: /usr/local/lib/libopencv_saliency.so.3.1.0
../crop: /usr/local/lib/libopencv_rgbd.so.3.1.0
../crop: /usr/local/lib/libopencv_reg.so.3.1.0
../crop: /usr/local/lib/libopencv_plot.so.3.1.0
../crop: /usr/local/lib/libopencv_optflow.so.3.1.0
../crop: /usr/local/lib/libopencv_line_descriptor.so.3.1.0
../crop: /usr/local/lib/libopencv_fuzzy.so.3.1.0
../crop: /usr/local/lib/libopencv_face.so.3.1.0
../crop: /usr/local/lib/libopencv_dpm.so.3.1.0
../crop: /usr/local/lib/libopencv_dnn.so.3.1.0
../crop: /usr/local/lib/libopencv_datasets.so.3.1.0
../crop: /usr/local/lib/libopencv_ccalib.so.3.1.0
../crop: /usr/local/lib/libopencv_bioinspired.so.3.1.0
../crop: /usr/local/lib/libopencv_bgsegm.so.3.1.0
../crop: /usr/local/lib/libopencv_aruco.so.3.1.0
../crop: /usr/local/lib/libopencv_videostab.so.3.1.0
../crop: /usr/local/lib/libopencv_videoio.so.3.1.0
../crop: /usr/local/lib/libopencv_video.so.3.1.0
../crop: /usr/local/lib/libopencv_superres.so.3.1.0
../crop: /usr/local/lib/libopencv_stitching.so.3.1.0
../crop: /usr/local/lib/libopencv_shape.so.3.1.0
../crop: /usr/local/lib/libopencv_photo.so.3.1.0
../crop: /usr/local/lib/libopencv_objdetect.so.3.1.0
../crop: /usr/local/lib/libopencv_ml.so.3.1.0
../crop: /usr/local/lib/libopencv_imgproc.so.3.1.0
../crop: /usr/local/lib/libopencv_imgcodecs.so.3.1.0
../crop: /usr/local/lib/libopencv_highgui.so.3.1.0
../crop: /usr/local/lib/libopencv_flann.so.3.1.0
../crop: /usr/local/lib/libopencv_features2d.so.3.1.0
../crop: /usr/local/lib/libopencv_core.so.3.1.0
../crop: /usr/local/lib/libopencv_calib3d.so.3.1.0
../crop: /usr/local/lib/libopencv_text.so.3.1.0
../crop: /usr/local/lib/libopencv_face.so.3.1.0
../crop: /usr/local/lib/libopencv_ximgproc.so.3.1.0
../crop: /usr/local/lib/libopencv_xfeatures2d.so.3.1.0
../crop: /usr/local/lib/libopencv_shape.so.3.1.0
../crop: /usr/local/lib/libopencv_video.so.3.1.0
../crop: /usr/local/lib/libopencv_objdetect.so.3.1.0
../crop: /usr/local/lib/libopencv_calib3d.so.3.1.0
../crop: /usr/local/lib/libopencv_features2d.so.3.1.0
../crop: /usr/local/lib/libopencv_ml.so.3.1.0
../crop: /usr/local/lib/libopencv_highgui.so.3.1.0
../crop: /usr/local/lib/libopencv_videoio.so.3.1.0
../crop: /usr/local/lib/libopencv_imgcodecs.so.3.1.0
../crop: /usr/local/lib/libopencv_imgproc.so.3.1.0
../crop: /usr/local/lib/libopencv_flann.so.3.1.0
../crop: /usr/local/lib/libopencv_core.so.3.1.0
../crop: CMakeFiles/crop.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../crop"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/crop.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/crop.dir/build: ../crop
.PHONY : CMakeFiles/crop.dir/build

CMakeFiles/crop.dir/requires: CMakeFiles/crop.dir/src/crop.cpp.o.requires
.PHONY : CMakeFiles/crop.dir/requires

CMakeFiles/crop.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/crop.dir/cmake_clean.cmake
.PHONY : CMakeFiles/crop.dir/clean

CMakeFiles/crop.dir/depend:
	cd /home/adam/Dropbox/UAS/uas/trainPREP/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/adam/Dropbox/UAS/uas/trainPREP /home/adam/Dropbox/UAS/uas/trainPREP /home/adam/Dropbox/UAS/uas/trainPREP/build /home/adam/Dropbox/UAS/uas/trainPREP/build /home/adam/Dropbox/UAS/uas/trainPREP/build/CMakeFiles/crop.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/crop.dir/depend

