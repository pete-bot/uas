#ifndef SALIENCYFILTER_H_
#define SALIENCYFILTER_H_

class SaliencyFilter
{
public:
	//SaliencyFilter();
	void ApplySaliencyFilter(cv::Mat& input, cv::Mat& output);
	//std::vector<cv::Rect> GetSalientROI(cv::Mat& input);
private:	
	void ThreshSaliency(cv::Mat& sal, cv::Mat& output);
};

#endif /*SALIENCYFILTER_H_ */
