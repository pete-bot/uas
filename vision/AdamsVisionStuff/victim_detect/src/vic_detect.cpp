#include <iostream>
#include <cstdlib>
#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/saliency.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv::saliency;

bool LoadVideo(std::string video_name, cv::VideoCapture& capture);
void ApplySaliencyFilter(cv::Mat& input, cv::Mat& output);
void ThreshSaliency(cv::Mat& sal, cv::Mat& output);
void ApplyBING(cv::Mat& input, cv::Mat& output);
void GenSaliencyMap(cv::Mat& input, cv::Mat& output);
std::vector<cv::Rect> GetBoundingROI(cv::Mat& thresh, cv::Mat src);
void detectAndDisplay(cv::Mat& roi,cv::Mat& frame, cv::Rect roiRect);
void kmeanSeg(cv::Mat& sal_img, cv::Mat& output);

std::string full_body_cascade_name = "haarcascade_fullbody.xml";
std::string upper_body_cascade_name = "haarcascade_upperbody.xml";
std::string s6_cascade_name = "cascade.xml";
std::string s7_cascade_name = "cascade_1.xml";
std::string s11_cascade_name = "cascade_2.xml";
std::string s16_cascade_name = "cascade_3.xml";
cv::CascadeClassifier full_cascade;
cv::CascadeClassifier upper_cascade;
cv::CascadeClassifier s6_cascade;
cv::CascadeClassifier s7_cascade;
cv::CascadeClassifier s11_cascade;
cv::CascadeClassifier s16_cascade;

int main(int argc, char** argv )
{
	
	if(argc < 2) {
		std::cout << "Usage: ./exec <videoFileName>" << std::endl;
		return -1;
	}
	
	cv::VideoCapture cap;
	bool loaded_video = LoadVideo(argv[1], cap);
	
	if(!loaded_video) {
		std::cout << "Exiting" << std::endl;
		return -1;
	}
	
	if(!full_cascade.load(full_body_cascade_name)) {
		std::cout << "Error loading full body" << std::endl;
		return -1;
	}
	
	if(!upper_cascade.load(upper_body_cascade_name)) {
		std::cout << "Error loading upper body" << std::endl;
		return -1;
	}
	
	if(!s6_cascade.load(s6_cascade_name)) {
		std::cout << "Error loading s6" << std::endl;
		return -1;
	}
	
	if(!s7_cascade.load(s7_cascade_name)) {
		std::cout << "Error loading s7" << std::endl;
		return -1;
	}
	
	if(!s11_cascade.load(s11_cascade_name)) {
		std::cout << "Error loading s11" << std::endl;
		return -1;
	}
	
	if(!s16_cascade.load(s16_cascade_name)) {
		std::cout << "Error loading s16" << std::endl;
		return -1;
	}
	
	
	while(true) {
		
		cv::Mat frame;
		
		bool read_frame = cap.read(frame); 
		
		if(!read_frame) {
			std::cout << "End of video" << std::endl;
			break;
			
		}
		
		
		cv::Mat cp_image;
		cv::Mat sal_image;
		cv::Mat thresh_image;
		
		cv::Mat frame_cp;
		frame.copyTo(frame_cp);
		GenSaliencyMap(frame_cp, cp_image);
		ThreshSaliency(cp_image, thresh_image);
		std::vector<cv::Rect> roi = GetBoundingROI(thresh_image, frame); 
		
		
		cv::Mat detect;
		frame.copyTo(detect);
		for(size_t i = 0; i < roi.size(); i++) {
					
			cv::Mat crop;
			frame(roi[i]).copyTo(crop);
			detectAndDisplay(crop,detect, roi[i]);	
		}
			
		imshow("Detect", detect);	
		//imshow("Output", frame);
		imshow("Custom Saliency", cp_image);
		//imshow("Thresh", thresh_image);
		
		if(cv::waitKey(30) == 27) {
			break;
		}
		
	}
	
	cv::waitKey(0);
	return 0;
	
}

bool LoadVideo(std::string video_name, cv::VideoCapture& capture) {
	
	cv::VideoCapture cap(video_name); 
	
	if(!cap.isOpened()) {
		return false;
	}
	
	capture = cap;
	
	return true;
	
} 


void detectAndDisplay(cv::Mat& roi,cv::Mat& frame, cv::Rect roiRect) 
{
	
	std::vector<cv::Rect> bodies;
	cv::Mat roi_gray;
	
	cv::cvtColor(roi, roi_gray, CV_BGR2GRAY);
	cv::equalizeHist(roi_gray, roi_gray);
	
	s11_cascade.detectMultiScale(roi_gray, bodies, 1.2,3, 0|CV_HAAR_SCALE_IMAGE, cv::Size(30,30));
	
	
	if(bodies.size() >= 1 && bodies.size() <= 10) {
		rectangle(frame, roiRect, cv::Scalar(255,0,255));
		for(size_t i = 0; i < bodies.size(); i++) 
		{
			cv::Rect temp(cv::Point(roiRect.tl().x+bodies[i].x,roiRect.tl().y+bodies[i].y), roiRect.br());
			
			rectangle(frame, temp, cv::Scalar(0,255,0));
		}
	}
	
}



void GenSaliencyMap(cv::Mat& input, cv::Mat& output)
{
	
	cv::Mat cp_image;
	cv::Mat gaus_image;
	input.copyTo(cp_image); 
	input.copyTo(gaus_image);
	
	cv::cvtColor(cp_image,cp_image,CV_RGB2Lab);
	cv::Scalar m_scalar = cv::mean(cp_image);
	
	cv::cvtColor(gaus_image,gaus_image,CV_RGB2Lab);
	cv::GaussianBlur(gaus_image, gaus_image,cv::Size(7,7),0,0);
	
	cp_image.setTo(m_scalar);
	
	cv::Mat diffSum;
	cv::Mat diffImage;
	cv::absdiff(cp_image, gaus_image,diffImage); 
	imshow("Diff", diffImage);
	//imshow("Gaus", gaus_image);
	
	std::vector<cv::Mat> channels(3);
	cv::split(diffImage,channels);
	
	cv::addWeighted(channels[0],1,channels[1],1,0.0,diffSum);
	cv::addWeighted(channels[2],1,diffSum,1,0.0,diffSum);
		
	output = diffSum*2;
	
	//cv::Mat sal_cp;
	//output.copyTo(sal_cp);
	//cv::Mat k_output;
	//kmeanSeg(sal_cp, k_output);
	
	
}

void kmeanSeg(cv::Mat& sal_img, cv::Mat& output) 
{
	
	cv::blur(sal_img, sal_img, cv::Size(15,15));
	
	cv::Mat k_input = cv::Mat::zeros(sal_img.cols*sal_img.rows,5,CV_32F);
	cv::Mat best_labels, centers, clustered;
	
	std::vector<cv::Mat> bgr;
	cv::split(sal_img, bgr);
	
	for(int i =0; i < sal_img.cols*sal_img.rows; i++) {
		k_input.at<float>(i,0) = (i/sal_img.cols)/sal_img.rows;
		k_input.at<float>(i,1) = (i%sal_img.cols)/sal_img.cols;
		k_input.at<float>(i,2) = bgr[0].data[i] / 255.0;
		k_input.at<float>(i,3) = bgr[1].data[i] / 255.0;
		k_input.at<float>(i,4) = bgr[2].data[i] / 255.0;
	}
	
	int K = 2;
	cv::kmeans(k_input, K, best_labels, 
			cv::TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 10, 1.0),
			3, cv::KMEANS_PP_CENTERS, centers);
	
	int colors[K];
	
	for(int i = 0; i < K; i++) {
			colors[i] = 255/(i+1);
	}
	
	clustered = cv::Mat(sal_img.rows, sal_img.cols, CV_32F);
	for(int i = 0; i < sal_img.rows*sal_img.cols; i++) {
		clustered.at<float>(i/sal_img.cols, i%sal_img.cols) = (float)(colors[best_labels.at<int>(0,i)]);
	}
	
	clustered.convertTo(clustered, CV_8U);

	imshow("clustered", clustered);
	
	output = clustered;
	
	 
	
}

void ThreshSaliency(cv::Mat& sal, cv::Mat& output) 
{
	int morph_size = 5;
	cv::Mat element = getStructuringElement(cv::MORPH_RECT, cv::Size(2*morph_size+1, 2*morph_size+1), cv::Point(morph_size, morph_size));
	cv::Mat thresh;
	//cv::blur(sal, sal, cv::Size(7,7));

	cv::threshold(sal,thresh,200,255,CV_THRESH_BINARY);//|CV_THRESH_OTSU);
	cv::morphologyEx(thresh,thresh,cv::MORPH_DILATE,element, cv::Point(-1,-1));
	output = thresh;
	imshow("Thresh", thresh);
}

std::vector<cv::Rect> GetBoundingROI(cv::Mat& thresh, cv::Mat src) 
{
	cv::Size s = src.size();
	
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	
	findContours(thresh,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE,cv::Point(0,0));
	
	std::vector<std::vector<cv::Point>> contours_poly (contours.size());
	std::vector<cv::Rect> boundRect;
	
	for(unsigned int i = 0; i < contours.size(); i++) {
		approxPolyDP(cv::Mat(contours[i]), contours_poly[i], 3, true);
		
		cv::Rect rect_res = boundingRect(cv::Mat(contours_poly[i]));
		
		if(rect_res.width >= s.width-5 && rect_res.height >= s.height-5) {
			continue;
		}
		
		boundRect.push_back(rect_res);
	}
	
	cv::Mat output;
	src.copyTo(output);
	
	for(unsigned int i = 0; i < contours.size(); i++) {
		
		int inc = 25;
		cv::Point top = boundRect[i].tl();
		cv::Point bottom = boundRect[i].br();
		if(top.x-inc > 0  && top.y-inc > 0 && bottom.x+inc < src.cols && bottom.y+inc < src.rows) {
			
			top.x -= inc;
			top.y -= inc;
			bottom.x += inc;
			bottom.y += inc;
			boundRect[i] = cv::Rect(top,bottom);
		}
		
		cv::rectangle(output, boundRect[i].tl(), boundRect[i].br(),cv::Scalar(255,0,0),2,8,0);
	}
	
	imshow("Bounded", output);
	
	return boundRect;
	
}




