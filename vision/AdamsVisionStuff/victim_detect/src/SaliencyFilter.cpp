#include <iostream>
#include <stdio.h>
#include <cstdlib>

#include "BaseCV.h"
#include "SaliencyFilter.h"

using namespace cv::saliency;

//SaliencyFilter::SaliencyFilter() {}

void SaliencyFilter::ApplySaliencyFilter(cv::Mat& input, cv::Mat& output) 
{
	cv::Ptr<Saliency> saliency_algorithm = Saliency::create( "SPECTRAL_RESIDUAL" );
	cv::Mat saliency_map;
    if( saliency_algorithm->computeSaliency( input, saliency_map ) )
    {
      output = saliency_map;
    }
	
}

/*
std::vector<cv::Rect> SaliencyFilter::GetSalientROI(cv::Mat& input) 
{
	
}
*/

void SaliencyFilter::ThreshSaliency(cv::Mat& sal, cv::Mat& output) 
{
	cv::Mat binary_map;
	StaticSaliencySpectralResidual spec;
	spec.computeBinaryMap( sal, binary_map);
	output = binary_map;
}


