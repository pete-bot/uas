// peter kydd pkydd@cse.unsw.edu.au
// 29/04/2016
// program to pull video image data from generic USB camera. 

#include <iostream>
#include <cstdlib>

// opencv includes
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

// our local includes
#include <camera_lib.h>


int main(int argc, char* argv[] )
{


	// create camera object
	
	cv::VideoCapture cap(1); // open the default camera
    if(!cap.isOpened()) {return -1;} // check if we succeeded
	
	// set resolution of camera .
	cap.set(CV_CAP_PROP_FRAME_WIDTH,320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,240);        

    //cv::namedWindow("input",CV_WINDOW_NORMAL);
    cv::namedWindow("output",CV_WINDOW_NORMAL);
    

    // capture loop
    while(true){

    	
    	cv::Mat input, output; // create frame matrix
    	cap >> input;  // get data from cap, load into frame

        // draw crosshair
        output = drawCrosshairs(input);
        //cv::imshow("input", input);
        cv::imshow("output", output);

        //showYellowTestBox();

        // allow user to exit capture
        short c = cv::waitKey(1); 
        if ( c == 'q' ){
            std::cout << "Stopping capture..." << std::endl;
            break;
        }
    }


    return EXIT_SUCCESS;
}




