// peter kydd pkydd@cse.unsw.edu.au
// 07/02/2016
// program to pull video image data from cham3 camera and produce canny edge emagery. 

#include <iostream>
#include <cstdlib>
#include "FlyCapture2.h"

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>


cv::Mat drawCrosshairs(cv::Mat input);
void showYellowTestBox(void);
void showCameraInfo(const FlyCapture2::CameraInfo& camInfo);
void printCameraInfo( const FlyCapture2::CameraInfo& pCamInfo );



int main(int argc, char* argv[] )
{

    // establish connection to camera
    FlyCapture2::Error error;
    FlyCapture2::Camera camera;
    FlyCapture2::CameraInfo camInfo;

    // Connect the camera
    // param is PGRGuid* pGuid  - the guid of the imaging device. default is NULL or 0 (for first cam)
    error = camera.Connect( 0 );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cerr << "ERROR: Failed to connect to camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // Get the camera info and print it out
    error = camera.GetCameraInfo( &camInfo );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cout << "ERROR: Failed to get camera info from camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // start capturing from device          
    error = camera.StartCapture();
    if ( error == FlyCapture2::PGRERROR_ISOCH_BANDWIDTH_EXCEEDED ){
        std::cout << "ERROR: Bandwidth exceeded." << std::endl;     
        return EXIT_FAILURE;
    } else if ( error != FlyCapture2::PGRERROR_OK ){
        std::cout << "ERROR: Failed to start image capture." << std::endl;     
        return EXIT_FAILURE;
    }          


    // create viewer window (resizable)
    cv::namedWindow("input",CV_WINDOW_NORMAL);


    std::cout << "Press SPACE to capture colouor vector (bgr) at crosshair." << std::endl;

    int count = 0;
    // capture loop
    while(true){
        
        // get and covert image from flycap chameleon to opencv format
        // Get the image        
        FlyCapture2::Image rawImage;
        FlyCapture2::Error error = camera.RetrieveBuffer( &rawImage );
        if ( error != FlyCapture2::PGRERROR_OK ){
            std::cout << "ERROR: capture error." << std::endl;
            continue;
        }
        
        // convert to rgb
        FlyCapture2::Image rgbImage;
        rawImage.Convert( FlyCapture2::PIXEL_FORMAT_BGR, &rgbImage );
       
        // convert to OpenCV Mat
        unsigned int rowBytes = (double)rgbImage.GetReceivedDataSize()/(double)rgbImage.GetRows();       
        cv::Mat input;
        input = cv::Mat(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC3, rgbImage.GetData(),rowBytes);

        cv::Mat inClone = input.clone();
        input = drawCrosshairs(input);

        imshow("input", input);


        short c = cv::waitKey(1); 
        // take a snapshot or exit capture mode
        if(  c == ' ' ){
            std::cout << "Space Detected..." << std::endl;
            
            // capture data
            cv::Vec3b bgrPixel = inClone.at<cv::Vec3b>(inClone.rows/2, inClone.cols/2);
            std::cout << count << ": " << bgrPixel  << std::endl; 
            count++;

            cv::Mat target_colour(50,50,CV_8UC3, bgrPixel);
            std::stringstream ss;
            ss << "colour_samples/[" << std::to_string(bgrPixel[0]) << "," 
                << std::to_string(bgrPixel[1]) << ","
                << std::to_string(bgrPixel[2])<<"]" <<".jpg";
            
            imwrite( ss.str(), target_colour );



        } else if ( c == 'q' ){
            std::cout << "Stopping capture..." << std::endl;
            break;
        }
    }

    // stop capture
    error = camera.StopCapture();
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cerr << "ERROR: Camera did not stop capture cleanly." << std::endl;
        return EXIT_FAILURE;
    }  

    // clean up camera buffers etc and disconnect camera from camera object. 
    camera.Disconnect();


    return EXIT_SUCCESS;
}




cv::Mat drawCrosshairs(cv::Mat input)
{

    cv::Scalar red(0,0,255);
    //crosshair horizontal
    cv::line(input, cv::Point((input.cols/2)-40, input.rows/2), cv::Point((input.cols/2)+40, input.rows/2), red, 1, cv::LINE_4,0);  

    //crosshair vertical
    cv::line(input, cv::Point(input.cols/2, (input.rows/2)-40), cv::Point(input.cols/2, (input.rows/2)+40), red, 1, cv::LINE_4,0);  

    return input;
}


