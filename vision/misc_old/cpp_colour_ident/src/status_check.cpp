// peter kydd pkydd@cse.unsw.edu.au
// 07/02/2016
// program to pull video image data from cham3 camera and produce canny edge emagery. 

#include <iostream>
#include <cstdlib>
#include "FlyCapture2.h"

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

void printCameraInfo( const FlyCapture2::CameraInfo& pCamInfo );

int main(int argc, char* argv[] )
{

    // establish connection to camera
    FlyCapture2::Error error;
    FlyCapture2::Camera camera;
    FlyCapture2::CameraInfo camInfo;

    // Connect the camera
    // param is PGRGuid* pGuid  - the guid of the imaging device. default is NULL or 0 (for first cam)
    error = camera.Connect( 0 );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cerr << "ERROR: Failed to connect to camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // Get the camera info and print it out
    error = camera.GetCameraInfo( &camInfo );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        std::cout << "ERROR: Failed to get camera info from camera." << std::endl;     
        return EXIT_FAILURE;
    }

    // display camera data (not really needed)
    printCameraInfo(camInfo);

    // clean up camera buffers etc and disconnect camera from camera object. 
    camera.Disconnect();


    return EXIT_SUCCESS;
}


void printCameraInfo( const FlyCapture2::CameraInfo& pCamInfo )
{
    std::cout << std::endl;
    std::cout <<  "CAMERA STATUS: OK..." << std::endl << std::endl;

    std::cout << "*** CAMERA INFORMATION ***" << std::endl;
    std::cout << "Serial number     - " << pCamInfo.serialNumber << std::endl;
    std::cout << "Camera model      - " << pCamInfo.modelName << std::endl;
    std::cout << "Camera vendor     - " << pCamInfo.vendorName << std::endl;
    std::cout << "Sensor            - " << pCamInfo.sensorInfo << std::endl;
    std::cout << "Resolution        - " << pCamInfo.sensorResolution << std::endl;
    std::cout << "Firmware version  - " << pCamInfo.firmwareVersion << std::endl;
    std::cout << "Firmware build    - " << pCamInfo.firmwareBuildTime << std::endl << std::endl;
}

