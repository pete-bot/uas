// peter kydd pkydd@cse.unsw.edu.au
// 29/04/2016
// program to pull video image data from generic USB camera. 

#include <iostream>
#include <cstdlib>
#include <string>
#include <climits>


// our local includes
#include "camera_lib.h"


const bool CAPTURE_LIVE = true;
const double PI = atan2(0, -1);

// FLOW PARAMETERS
// resolution
const std::pair<int, int> RES(320,240);
const unsigned int FRAME_RATE = 60;

double max_thresh = INT_MIN;
double min_thresh = INT_MAX;
//const std::string FILENAME = "test_data/field_test_1.mp4";


inline double magnitude(const cv::Point2f & p); 

int main(int argc, char* argv[] )
{
    if(argc < 2){ 
        std::cout << "Please pass name of video file as argument ./test_1 <path/to/file>" << std::endl; 
        return -1;
    }
    const std::string FILENAME = argv[1];




    std::cout << "openCV: " << CV_MAJOR_VERSION << "." << CV_MINOR_VERSION  << std::endl;


    cv::VideoCapture cap(FILENAME); // open the default camera
    
    if(CAPTURE_LIVE){

        // create camera object
        // read from USB_cam 
        
        // for camera input 
        //set resolution of camera .
    	cap.set(CV_CAP_PROP_FRAME_WIDTH, RES.first);
    	cap.set(CV_CAP_PROP_FRAME_HEIGHT, RES.second);        
        cap.set(CV_CAP_PROP_FPS, FRAME_RATE);
        
    }

    if(!cap.isOpened()) {
        std::cerr << "VideoCapture FAILED." << std::endl; 
        return -1;
    } // check if we succeeded

    // UMat is a Mat type that is allocated to CPU/GPU etc dynamically to optimise perf.
    cv::Mat flow, frame, input;
    cv::UMat flowUmat, prevgray;
    cv::Mat img, original; // create frame matrix


        
    cap >> img;
    // resize
    cv::resize(img, img, cv::Size(RES.first, RES.second));
    img.copyTo(original);
    img.copyTo(input);
    
    cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);
    
    double mag = 0;


    // capture loop
    while(true){

        // fill previous image again
    	img.copyTo(prevgray);

        // get data from cap, load into frame
        cap >> img;  
        if(img.empty()){ std::cout << "End of input stream." << std::endl; break;}
        
        // resize
        cv::resize(img, img, cv::Size(RES.first, RES.second));
        img.copyTo(original);
        img.copyTo(input);

        // convert to mono
        cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);

        // if prev was not initialised, initialise and step ahead
        
            
        //calculate optical flow 
        cv::calcOpticalFlowFarneback(prevgray, img, flowUmat, 0.5, 1, 12, 2, 5, 1.2, 0);

        //copy Umat container to standard Mat
        flowUmat.copyTo(flow);

        // double angle;
        // double mag;

        // By y += 5, x += 5 you can specify the grid 
        for (int y = 0; y < original.rows; y += 10){
            
            for (int x = 0; x < original.cols; x += 10){
                
                // grab flow data at point x,y in image
                const cv::Point2f& flow_xy=flow.at<cv::Point2f>(y,x) * 10;
                

                // angle=cv::fastAtan2(flow_xy.x,flow_xy.y);
                // if(angle<0) angle+=2*PI;

                // mag = magnitude(flow_xy);



    
                
                // // get the flow from y, x position * 10 for better visibility
                // const cv::Point2f flow_xy = flow.at<cv::Point2f>(y, x) * 10;
                
                cv::line(original, cv::Point(x, y), cv::Point(cvRound(x + flow_xy.x), 
                    cvRound(y + flow_xy.y)), cv::Scalar(0,0,255), 1);
                
                // // draw initial point
                cv::circle(original, cv::Point(x, y), 1, cv::Scalar(0, 0, 0), -1);
            
                
            }
        }



        //cv::cvtColor(color_map, color_map, cv::COLOR_HSV2BGR);


        // draw the results
        // cv::namedWindow("color_map", CV_WINDOW_AUTOSIZE);
        // cv::imshow("color_map", color_map);


        // draw the results
        cv::namedWindow("original",CV_WINDOW_NORMAL);
        cv::imshow("original", original);

        // draw the results
        cv::namedWindow("input",CV_WINDOW_NORMAL);
        cv::imshow("input", input);







        // allow user to pause/exit capture/playback
        short c = cv::waitKey(1);
        bool quit_flag = false; 
        if ( c == 'p' ){
            std::cout << "PAUSE" << std::endl;
            c = 0;

            while(c != 'p'){
                c = cv::waitKey(1);
                if(c == 'q'){quit_flag = true; break;}
            }
            std::cout << "UN-PAUSING" << std::endl;
            if(quit_flag) {
                std::cout << "Stopping playback..." << std::endl;
                
                break;
            }
        } else if (c == 'q') {
            std::cout << "Stopping playback..." << std::endl;
            break;
        }

    }

    std::cout << "max_mag: " << mag << std::endl;


    return EXIT_SUCCESS;
}


inline double magnitude(const cv::Point2f & p) 
{
    double mag = sqrt(p.x*p.x + p.y*p.y); 
    
    return mag;

}
